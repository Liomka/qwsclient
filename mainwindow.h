#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtCore/QDateTime>
#include <QtWidgets/QMainWindow>

#include "websocketclient.h"

namespace Ui {
class MainWindow;
}

class ConnectionDialog;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void accepted(QDateTime);
    void rejected(QDateTime);
    void disconnected();
    void userListReceived(QList<User> userListReceived, QDateTime time);
    void userConnected(User user, QDateTime time);
    void messageReceived(Message);
    void userNameChanged(QString id, QString newName, QDateTime time);
    void historyReceived(QDateTime time);

    void on_chatUserInput_returnPressed();

private: // Enums
    enum MESSAGETYPE {
        NORMAL,
        EVENT,
        WARNING,
        ERROR,
        SYSTEM
    };

private: // Methods
    void appendInChat(QString messageReceived, MESSAGETYPE type);

private: // Properties
    Ui::MainWindow *ui;

    WebSocketClient *_webSocket;
    ConnectionDialog *_connectionDialog;

    QList<User> _connectedUsers;
};

#endif // MAINWINDOW_H
