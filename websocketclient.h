#ifndef WEBSOCKETCLIENT_H
#define WEBSOCKETCLIENT_H

#include <QtCore/QList>
#include <QtCore/QDateTime>

#include <QtWebSockets/QWebSocket>

class User {
public:
    QString id;
    QString name;

    User& setId(QString idIn) { id = idIn; return *this;}
    User& setName(QString nameIn) { name = nameIn; return *this;}
};

class Message {
public:
    QString id;
    QString name;
    QString content;
    QDateTime time;

    Message& setId(QString idIn) { id = idIn; return *this;}
    Message& setName(QString nameIn) { name = nameIn; return *this;}
    Message& setContent(QString contentIn) { content = contentIn; return *this;}
    Message& setTime(QDateTime timeIn) { time = timeIn; return *this;}
};

class WebSocketClient : public QWebSocket
{
    Q_OBJECT
public:
    explicit WebSocketClient(QObject *parent = 0);
    ~WebSocketClient();

    void sendMessage(QString messageContent);

public slots:
    void tryConnectWithName(QString name);

signals:
    void rejected(QDateTime time);
    void accepted(QDateTime time);
    void disconnected();
    void userList(QList<User>, QDateTime time);
    void userConnected(User, QDateTime time);
    void message(Message message);
    void nick(QString userId, QString newName, QDateTime time);
    void history(QDateTime time);

    void parseError(QString str);

private slots:
    void onConnect();
    void onTextMessageReceived(QString message);
    void onDiconnect();

private:
    QString _name;
    bool _connected;

};

#endif // WEBSOCKETCLIENT_H
