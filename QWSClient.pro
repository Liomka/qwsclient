#-------------------------------------------------
#
# Project created by QtCreator 2014-06-17T11:19:33
#
#-------------------------------------------------

QT       += core gui websockets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QWebSocketClient
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    connectiondialog.cpp \
    websocketclient.cpp

HEADERS  += mainwindow.h \
    connectiondialog.h \
    websocketclient.h

FORMS    += mainwindow.ui \
    connectiondialog.ui
