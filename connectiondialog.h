#ifndef CONNECTIONDIALOG_H
#define CONNECTIONDIALOG_H

#include <QtWidgets/QDialog>

namespace Ui {
class ConnectionDialog;
}

class QWebSocket;

class ConnectionDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ConnectionDialog(QWidget *parent = 0);
    ~ConnectionDialog();

signals:
    void tryConnectionWithName(QString);
//    void tryConnectionWithUriAndName(QUrl, QString); // TODO Change server destination

private slots:
    void on_connectButton_clicked();

private:
    Ui::ConnectionDialog *ui;
};

#endif // CONNECTIONDIALOG_H
