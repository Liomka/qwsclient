#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QtCore/QJsonObject>
#include <QtCore/QJsonDocument>

#include <QtWidgets/QScrollBar>

#include "connectiondialog.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    _webSocket = new WebSocketClient(this);

    _webSocket->connect(_webSocket, SIGNAL(accepted(QDateTime)), this, SLOT(accepted(QDateTime)));

    _connectionDialog = new ConnectionDialog(this);
    _connectionDialog->connect(_connectionDialog, SIGNAL(tryConnectionWithName(QString)), _webSocket, SLOT(tryConnectWithName(QString)));
    _connectionDialog->show();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::accepted(QDateTime)
{
    _webSocket->connect(_webSocket, SIGNAL(rejected(QDateTime)),
                        this, SLOT(rejected(QDateTime)));
    _webSocket->connect(_webSocket, SIGNAL(disconnected()),
                        this, SLOT(disconnected()));
    _webSocket->connect(_webSocket, SIGNAL(userListReceived(QDateTime)),
                        this, SLOT(userListReceived(QDateTime)));
    _webSocket->connect(_webSocket, SIGNAL(userConnected(QDateTime)),
                        this, SLOT(userConnected(QDateTime)));
    _webSocket->connect(_webSocket, SIGNAL(messageReceived(Message)),
                        this, SLOT(messageReceived(Message)));
    _webSocket->connect(_webSocket, SIGNAL(userNameChanged(QString, QString, QDateTime)),
                        this, SLOT(userNameChanged(QString, QString, QDateTime)));
    _webSocket->connect(_webSocket, SIGNAL(historyReceived(QDateTime)),
                        this, SLOT(historyReceived(QDateTime)));
    _connectionDialog->accept();
}

void MainWindow::rejected(QDateTime)
{
    qDebug() << "rejected received";
}

void MainWindow::disconnected()
{
    qDebug() << "disconnected received";
}

void MainWindow::userListReceived(QList<User> userList, QDateTime time)
{
    foreach (User user, userList)
        qDebug() << user.id << " " << user.name;
    qDebug() << "userList received";
}

void MainWindow::userConnected(User user, QDateTime time)
{
    qDebug() << "userConnected: " << user.name;
}

void MainWindow::messageReceived(Message message)
{
    appendInChat("<a href=#>" + message.name + "</a>" + ": " + message.content, EVENT);
}

void MainWindow::userNameChanged(QString id, QString newName, QDateTime time)
{
    qDebug() << "nick received";
}

void MainWindow::historyReceived(QDateTime time)
{
    qDebug() << "history received";
}

// ---------------------------------------------------------------------------
// Ui Behavior
// ---------------------------------------------------------------------------

// Send message form UI to WebSocket
void MainWindow::on_chatUserInput_returnPressed()
{
    if (!ui->chatUserInput->text().trimmed().isEmpty())
        _webSocket->sendMessage(ui->chatUserInput->text());
    ui->chatUserInput->clear();
}

// Send message to UI
void MainWindow::appendInChat(QString message, MainWindow::MESSAGETYPE type)
{
    ui->chatLog->appendHtml(message);
    ui->chatLog->verticalScrollBar()->setValue(ui->chatLog->verticalScrollBar()->maximum());
}
