#include "websocketclient.h"

#include <QtCore/QJsonObject>
#include <QtCore/QJsonArray>
#include <QtCore/QJsonValue>
#include <QtCore/QJsonDocument>

WebSocketClient::WebSocketClient(QObject *parent)
    : QWebSocket("http://liomka.io", QWebSocketProtocol::VersionLatest, parent)
    , _name("")
    , _connected(false)
{
    connect(this, SIGNAL(connected()),
            this, SLOT(onConnect()),
            static_cast<Qt::ConnectionType>(Qt::AutoConnection | Qt::UniqueConnection));
    connect(this, SIGNAL(textMessageReceived(QString)),
            this, SLOT(onTextMessageReceived(QString)),
            static_cast<Qt::ConnectionType>(Qt::AutoConnection | Qt::UniqueConnection));
    connect(this, SIGNAL(disconnected()),
            this, SLOT(onDiconnect()),
            static_cast<Qt::ConnectionType>(Qt::AutoConnection | Qt::UniqueConnection));
}

WebSocketClient::~WebSocketClient()
{
}

// ---------------------------------------------------------------------------
// PUBLIC METHODS
// ---------------------------------------------------------------------------
void WebSocketClient::sendMessage(QString messageContent)
{
    QJsonObject json;
    json["method"] = "message";
    json["content"] = messageContent;

    sendBinaryMessage(QJsonDocument(json).toJson());
}

// ---------------------------------------------------------------------------
// PUBLIC SLOTS
// ---------------------------------------------------------------------------
void WebSocketClient::tryConnectWithName(QString name) {
    qDebug() << "tryConnectWithName";
    _name = name;
    open(QUrl("ws://localhost:5000/ws")); // TODO Could fail here. emit fail ?
}

// ---------------------------------------------------------------------------
// PRIVATE WEBSOCKET BEHAVIOUR
// ---------------------------------------------------------------------------
void WebSocketClient::onConnect()
{
    QJsonObject json;
    json["method"] = "hello";
    json["name"] = _name;

    sendBinaryMessage(QJsonDocument(json).toJson());
}

void WebSocketClient::onTextMessageReceived(QString received)
{
    QJsonObject json = QJsonDocument::fromJson(received.toUtf8()).object();
    if (json.contains("method")) {
        QDateTime receivedTime = QDateTime::fromMSecsSinceEpoch(json["time"].toDouble());
        QString method = json["method"].toString();
        if (method == "rejected") {
            emit rejected(receivedTime);
        } else if (method == "accepted") {
            emit accepted(receivedTime);
        } else if (method == "userList") {
            QList<User> userList;
            foreach (QJsonValue juser, json["content"].toArray())
                userList << User().setId(juser.toObject()["id"].toString()).setName(juser.toObject()["name"].toString());
//            emit userList(userList, receivedTime);
        } else if (method == "userConnected") {
            emit userConnected(User().setId(json["id"].toString()).setName(json["name"].toString())
                ,receivedTime);
        } else if (method == "message") {
            emit message(Message().setId(json["from"].toObject()["id"].toString())
                .setName(json["from"].toObject()["name"].toString())
                .setContent(json["content"].toString())
                .setTime(receivedTime));
        } else if (method == "nick") {
            emit nick(json["userId"].toString()
                ,json["newName"].toString()
                ,receivedTime);
        } else if (method == "history") {
            emit history(receivedTime);
        }
    } else {
        emit parseError(received);
    }
}

void WebSocketClient::onDiconnect()
{
    qDebug() << "onDisconnect";
    emit disconnected();
}
